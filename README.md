## visitors

Small REST API server for gathering and processing of of visitors data.

Technologies:

    Python == 3.5 - Main programming language
    Redis == 3.2.0 - Storage for caching last 100 visits
    Flask == 0.10.1 - A lightweight Python web framework
    marshmallow == 2.7.0 - Validation JSON Schema
    requests == 2.10.0 - Library for making queries to archive server


Install docker on your Mac

    brew update
    brew install docker
    brew install docker-machine
    brew install docker-compose

Following versions should work:

Docker version 1.10.2, build c3959b1
docker-compose version 1.6.2, build unknown
docker-machine version 0.6.0, build e27fb87

Install docker machine NFS driver for proper live update (only for development)

    sudo curl -o /usr/local/bin/docker-machine-nfs https://raw.githubusercontent.com/adlogix/docker-machine-nfs/master/docker-machine-nfs.sh
    sudo chmod +x /usr/local/bin/docker-machine-nfs

Creating docker-machine with vagrant backend...

If you run virtualbox:
    docker-machine create -d virtualbox VisitorsDev
If you run vmware fusion:
    docker-machine create -d vmwarefusion VisitorsDev

Activates NFS for an existing boot2docker box created through Docker Machine:
    docker-machine-nfs VisitorsDev

Activating newly created machine...

    eval "$(docker-machine env VisitorsDev)"

Building docker containers (dev)...

    docker-compose build

Building docker containers (production)...

    docker-compose -f docker-compose-production.yml build

Running containers (dev)...

    docker-compose up -d

Running containers (production)...

    docker-compose -f docker-compose-production.yml up -d

Everything done. Application should be avaliable on IP

    docker-machine ip VisitorsDev

Running tests

    docker-compose run --rm api nosetests

Fill some visits data for testing frontend

    docker-compose run --rm api python manage.py fill_data  <server__IP:server__port>


Clear cache. It will remove all visits from redis

    docker-compose run --rm api python manage.py clear_cache

Settings like `ARCHIVE_SERVER` or `TOKEN` could be changed in `.environment` or `.environment-production`

