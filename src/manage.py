#!/usr/bin/env python
from random import randint
from urllib.parse import urljoin

from flask.ext.script import Manager, Shell

import requests

from visitors.app import create_app
from visitors.config import Config
from visitors.extensions import r


app = create_app()
manager = Manager(app)


def make_shell_context():
    return {'app': app}

manager.add_command("shell", Shell(make_context=make_shell_context))


@manager.command
def clear_cache():
    """
    This function will delete all cached visits from redis
    """
    r.flushall()


@manager.option(
    '-u', '--url', help='Base url to the running server. For example http://192.168.99.100:8000/'
)
@manager.command
def fill_data(url):
    """
    Fill in some visits data to a system for a testing reason. They will be shown on the map.
    """
    visits = []
    for _ in range(1000):
        visits.append({
            'lat': 59.330842 + randint(-300, 300) / 1000000,
            'long': 18.071806 + randint(-300, 300) / 1000000,
            'time': 1458572383 + randint(-300, 300),
            'device': '727d2a99b62d5759',
            'rssi': 0 - randint(50, 95)
        })

    response = requests.post(
        urljoin(url, '/API/track/'),
        json={
            'node_type': 3,
            'visits': visits
        },
        headers={'content-type': 'application/json', 'X-Auth-Token': Config.TOKEN}
    )

    response = requests.post(
        urljoin(url, '/API/track/'),
        json={
            'node_type': 3,
            'visits': [
                {'lat': 59.328464, 'long': 18.066829, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
                {'lat': 59.328458, 'long': 18.066802, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
                {'lat': 59.328473, 'long': 18.066804, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
                {'lat': 59.328446, 'long': 18.066829, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
                {'lat': 59.328424, 'long': 18.066672, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
                {'lat': 59.328416, 'long': 18.066651, 'time': 1458572383 + randint(-300, 300),
                 'device': '727d2a99b62d5759', 'rssi': 0 - randint(50, 95)},
            ]
        },
        headers={'content-type': 'application/json', 'X-Auth-Token': Config.TOKEN}
    )
    print('Success!')

if __name__ == '__main__':
    manager.run()
