Flask==0.10.1
Flask-Script==2.0.5
gunicorn==19.3.0
itsdangerous==0.24
Jinja2==2.8
pytz==2015.4
marshmallow==2.7.0
factory_boy==2.7.0
requests==2.10.0
redis==2.10.5

ipython==2.4.1
ipdb==0.8
Flask-Testing==0.4.2
nose==1.3.7
