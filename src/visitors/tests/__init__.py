import logging

from flask.ext.testing import TestCase

from visitors.app import create_app

logger = logging.getLogger('base_test_case')


class BaseTestCase(TestCase):
    TESTING = True

    def create_app(self):
        app = create_app()
        app.config['TESTING'] = True
        return app
