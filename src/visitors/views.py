from flask import Blueprint, url_for
from flask import render_template

bp = Blueprint("main", __name__)


@bp.route('/viewer/', methods=['GET'])
def index():
    return render_template(
        'viewer.html'
    )
