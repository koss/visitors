import json
from random import randint
from unittest.mock import patch

from flask import url_for

from visitors.tests import BaseTestCase
from visitors.config import Config
from visitors.extensions import r


TOKEN = Config.TOKEN


class TracksTest(BaseTestCase):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()
        r.flushall()

    def test_add_visits_missing_node_type(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'visits': [
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.231, 'long': 25.5, 'time': 1458572381,
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.54, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -80},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 400)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('validation_error', data['status'])

        self.assertIn('errors', data)
        self.assertIn('node_type', data['errors'])
        self.assertIn('Missing data for required field.', data['errors']['node_type'])

    def test_add_visits_missing_token(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
            }),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 403)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('forbidden', data['status'])

    def test_add_visits_missing_visits(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 400)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('validation_error', data['status'])

        self.assertIn('errors', data)
        self.assertIn('visits', data['errors'])

        self.assertIn('0', data['errors']['visits'])
        self.assertIn('lat', data['errors']['visits']['0'])
        self.assertIn('Missing data for required field.', data['errors']['visits']['0']['lat'])

    def test_add_visits_invalid_visits(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                           'device': '727d2a99b62d5759', 'rssi': -60}
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 400)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('validation_error', data['status'])

        self.assertIn('errors', data)
        self.assertIn('visits', data['errors'])
        self.assertIn('Invalid type.', data['errors']['visits'])

    def test_add_visits_missing_visit_fields(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {'long': 25.543, 'time': 1458572383, 'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'time': 1458572383, 'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383, 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759'},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 400)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('validation_error', data['status'])

        self.assertIn('errors', data)
        self.assertIn('visits', data['errors'])
        self.assertIn('0', data['errors']['visits'])
        self.assertIn('1', data['errors']['visits'])
        self.assertIn('2', data['errors']['visits'])
        self.assertIn('3', data['errors']['visits'])
        self.assertIn('4', data['errors']['visits'])

        self.assertIn('Missing data for required field.', data['errors']['visits']['0']['lat'])
        self.assertIn('Missing data for required field.', data['errors']['visits']['1']['long'])
        self.assertIn('Missing data for required field.', data['errors']['visits']['2']['time'])
        self.assertIn('Missing data for required field.', data['errors']['visits']['3']['device'])
        self.assertIn('Missing data for required field.', data['errors']['visits']['4']['rssi'])

    def test_add_visits_wrong_visits(self):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {'lat': 'WRONG_LAT', 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 'WRONG_LONG', 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'time': 'WRONG_TIME',
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383, 'device': 0, 'rssi': -60},
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': 'WRONG_RSSI'},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 400)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('validation_error', data['status'])

        self.assertIn('errors', data)
        self.assertIn('visits', data['errors'])

        self.assertIn('0', data['errors']['visits'])
        self.assertIn('1', data['errors']['visits'])
        self.assertIn('2', data['errors']['visits'])
        self.assertIn('3', data['errors']['visits'])
        self.assertIn('4', data['errors']['visits'])

        self.assertIn('Not a valid number.', data['errors']['visits']['0']['lat'])
        self.assertIn('Not a valid number.', data['errors']['visits']['1']['long'])
        self.assertIn('Not a valid integer.', data['errors']['visits']['2']['time'])
        self.assertIn('Not a valid string.', data['errors']['visits']['3']['device'])
        self.assertIn('Not a valid integer.', data['errors']['visits']['4']['rssi'])

    @patch('visitors.api.v1.tracks.requests.post')
    def test_add_visits(self, request_post_mock):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -70},
                    {'lat': 51.231, 'long': 25.5, 'time': 1458572381,
                     'device': '727d2a99b62d5759', 'rssi': -50},
                    {'lat': 51.23, 'long': 25.54, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -80},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertEqual('ok', data['status'])

    @patch('visitors.api.v1.tracks.requests.post')
    def test_add_visits_archive(self, request_post_mock):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {"lat": 40.7143528, "long": -74.0059731, "time": 1458571183,
                     "device": "727d2a99b62d5759", "rssi": -50},
                    {"lat": 40.714010, "long": -74.006422, "time": 1458572183,
                     "device": "727d2a99b62d5759", "rssi": -70},
                    {"lat": 40.713669, "long": -74.006701, "time": 1458573183,
                     "device": "727d2a99b62d5759", "rssi": -85},
                    {"lat": 40.713148, "long": -74.007173, "time": 1458574183,
                     "device": "727d2a99b62d5759", "rssi": -55},

                    {"lat": 41.8781136, "long": -87.6297982, "time": 1458572381,
                     "device": "890d2a99b62d5759", "rssi": -80},
                    {"lat": 41.878163, "long": -87.629297, "time": 1458572686,
                     "device": "890d2a99b62d5759", "rssi": -65},
                    {"lat": 41.878139, "long": -87.628675, "time": 1458572883,
                     "device": "890d2a99b62d5759", "rssi": -75},

                    {"lat": 40.7143528, "long": -74.0059731, "time": 1458572987,
                     "device": "798d2a99b62d5759", "rssi": -90},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertEqual('ok', data['status'])
        self.assertEqual(request_post_mock.call_count, 3)

        request_post_mock.assert_any_call(
            'http://localhost:9090/api/archive/',
            json={
                'node_type': 'tracker',
                'time_start': 1458571183,
                'time_end': 1458574183,
                'samples_count': 4,
                'rssi_min': -85,
                'rssi_max': -50,
                'device': '727d2a99b62d5759',
                'average_position': {
                    'longitude': -74.00656727789658, 'latitude': 40.713794950818624
                }
            }
        )
        request_post_mock.assert_any_call(
            'http://localhost:9090/api/archive/',
            json={
                'node_type': 'tracker',
                'time_start': 1458572381,
                'time_end': 1458572883,
                'samples_count': 3,
                'rssi_min': -80,
                'rssi_max': -65,
                'device': '890d2a99b62d5759',
                'average_position': {
                    'latitude': 41.878138534248855, 'longitude': -87.62925673340003
                }
            }
        )
        request_post_mock.assert_any_call(
            'http://localhost:9090/api/archive/',
            json={
                'node_type': 'tracker',
                'time_start': 1458572987,
                'time_end': 1458572987,
                'samples_count': 1,
                'rssi_min': -90,
                'rssi_max': -90,
                'device': '798d2a99b62d5759',
                'average_position': {
                    'longitude': -74.0059731, 'latitude': 40.7143528
                }
            }
        )

    @patch('visitors.api.v1.tracks.requests.post')
    def test_add_visits_alias(self, request_post_mock):
        response = self.client.post(
            '/API/track/',
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {'lat': 51.23, 'long': 25.543, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -55},
                    {'lat': 51.231, 'long': 25.5, 'time': 1458572381,
                     'device': '727d2a99b62d5759', 'rssi': -60},
                    {'lat': 51.23, 'long': 25.54, 'time': 1458572383,
                     'device': '727d2a99b62d5759', 'rssi': -80},
                    {'lat': 59.326797, 'long': 18.080750, 'time': 1458582383,
                     'device': '727d2a99b62d5759', 'rssi': -80},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertEqual('ok', data['status'])

    @patch('visitors.api.v1.tracks.requests.post')
    def test_add_bunch_of_visits(self, request_post_mock):
        visits = []
        for _ in range(500):
            visits.append({
                "lat": 4.7143528 + randint(10, 90), "long": -4.0059731 + randint(10, 90),
                "time": 1458571183, "device": "727d2a99b62d5759", "rssi": -50
            })

        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': visits
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertEqual('ok', data['status'])

        response = self.client.get(
            url_for('api.v1.get_visits'),
            content_type='application/json',
        )
        self.assertEqual(response.status_code, 200)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertIn('objects', data)
        self.assertEqual('ok', data['status'])
        self.assertEqual(len(data['objects']), 100)

    @patch('visitors.api.v1.tracks.requests.post')
    def test_add_visits_duplicate(self, request_post_mock):
        response = self.client.post(
            url_for('api.v1.add_visits'),
            data=json.dumps({
                'node_type': 1,
                'visits': [
                    {"lat": 40.7143528, "long": -74.0059731, "time": 1458571183,
                     "device": "727d2a99b62d5759", "rssi": -50},
                    {"lat": 40.714010, "long": -74.006422, "time": 1458572183,
                     "device": "727d2a99b62d5759", "rssi": -70},
                    {"lat": 40.7143528, "long": -74.0059731, "time": 1458571183,
                     "device": "727d2a99b62d5759", "rssi": -70},
                    {"lat": 40.7143528, "long": -74.0059731, "time": 1458571183,
                     "device": "727d2a99b62d5759", "rssi": -60},

                    {"lat": 41.8781136, "long": -87.6297982, "time": 1458572381,
                     "device": "890d2a99b62d5759", "rssi": -80},
                    {"lat": 41.878163, "long": -87.629297, "time": 1458572686,
                     "device": "890d2a99b62d5759", "rssi": -65},
                    {"lat": 41.8781136, "long": -87.6297982, "time": 1458572381,
                     "device": "890d2a99b62d5759", "rssi": -63},
                ]
            }),
            content_type='application/json',
            headers={'X-Auth-Token': TOKEN}
        )
        self.assertEqual(response.status_code, 201)

        data = json.loads(response.data.decode('utf-8'))

        self.assertIn('status', data)
        self.assertEqual('ok', data['status'])
        self.assertEqual(request_post_mock.call_count, 2)

        request_post_mock.assert_any_call(
            'http://localhost:9090/api/archive/',
            json={
                'device': '727d2a99b62d5759',
                'node_type': 'tracker',
                'samples_count': 2,
                'time_start': 1458571183,
                'time_end': 1458572183,
                'rssi_max': -50,
                'rssi_min': -70,
                'average_position': {
                    'latitude': 40.71418140021736, 'longitude': -74.00619755057782
                }
            }
        )

        request_post_mock.assert_any_call(
            'http://localhost:9090/api/archive/',
            json={
                'device': '890d2a99b62d5759',
                'node_type': 'tracker',
                'samples_count': 2,
                'time_start': 1458572381,
                'time_end': 1458572686,
                'rssi_max': -63,
                'rssi_min': -65,
                'average_position': {
                    'latitude': 41.87813830027239, 'longitude': -87.62954760009686
                }
            }
        )

