import json

from json.decoder import JSONDecodeError
from operator import itemgetter

import requests
from flask import request

from visitors.api import bp as api_bp
from visitors.api.v1 import bp
from visitors.algolib import average_position
from visitors.algolib import get_last_visits
from visitors.algolib import get_unique_visits
from visitors.algolib import group_visits_by_device
from visitors.algolib import group_visits_by_position
from visitors.algolib import save_visits
from visitors.algolib import truncate_visit_position
from visitors.config import Config
from visitors.decorators import jsonify
from visitors.decorators import tracker_login_required
from visitors.serializers import NODE_TYPE_TRACKER, NODE_TYPE_INTERNET_PROVIDER
from visitors.serializers import InputTrackSchema


schema = InputTrackSchema()


@bp.route('/visits/', methods=['GET'])
@jsonify()
def get_visits():
    visits = [truncate_visit_position(x) for x in get_last_visits()]
    visits = [
        {'lat': lat, 'long': long, 'count': len(visits)}
        for (lat, long), visits in group_visits_by_position(visits).items()
    ]

    return {'status': 'ok', 'objects': visits}


@bp.route('/track/', methods=['POST'])
@api_bp.route('/track/', methods=['POST'])
@jsonify()
@tracker_login_required()
def add_visits():
    try:
        track_schema = schema.load(json.loads(request.data.decode('utf-8')))
    except JSONDecodeError:
        return {'status': 'validation_error', 'errors': ['Invalid JSON received']}, 400

    if track_schema.errors:
        return {'status': 'validation_error', 'errors': track_schema.errors}, 400

    data = track_schema.data

    unique_visits = get_unique_visits(data['visits'])

    save_visits(unique_visits)

    if data['node_type']['value'] in [NODE_TYPE_TRACKER, NODE_TYPE_INTERNET_PROVIDER]:
        for (device, device_visits) in group_visits_by_device(unique_visits).items():
            device_visits = list(device_visits)

            lat, long = average_position([(x['lat'], x['long']) for x in device_visits])

            requests.post(
                '{server}/api/archive/'.format(server=Config.ARCHIVE_SERVER),
                json={
                    "node_type": data['node_type']['title'],
                    "device": device,
                    "time_start": min(device_visits, key=itemgetter('time'))['time'],
                    "time_end": max(device_visits, key=itemgetter('time'))['time'],
                    "rssi_min": min(device_visits, key=itemgetter('rssi'))['rssi'],
                    "rssi_max": max(device_visits, key=itemgetter('rssi'))['rssi'],
                    "samples_count": len(device_visits),
                    "average_position": {"latitude": lat, "longitude": long}
                }
            )

    return {'status': 'ok'}, 201
