import json
import copy

from decimal import localcontext, ROUND_DOWN, Decimal
from collections import defaultdict
from functools import reduce
from operator import itemgetter
from math import radians, degrees, cos, sin, sqrt, atan2

from visitors.extensions import r


def to_cartesian(accumulator, point):
    x, y, z = accumulator
    lat, long = point
    return (
        x + cos(lat) * cos(long),
        y + cos(lat) * sin(long),
        z + sin(lat)
    )


def average_position(positions):
    """
    Returns average position from list of (lat, long)

    http://www.geomidpoint.com/example.html
    http://www.latlong.net/
    http://www.latlong.net/Show-Latitude-Longitude.html
    """
    if not positions:
        return 0, 0

    length = (float(len(positions)))

    positions = [(radians(lat), radians(long)) for (lat, long) in positions]

    x, y, z = reduce(to_cartesian, positions, (0, 0, 0))
    x, y, z = x / length, y / length, z / length

    long = atan2(y, x)
    lat = atan2(z, sqrt(x ** 2 + y ** 2))

    return degrees(lat), degrees(long)


def truncate_position(lat, long):
    with localcontext() as ctx:
        ctx.rounding = ROUND_DOWN
        return (
            float(Decimal(lat).quantize(Decimal('0.0001'))),
            float(Decimal(long).quantize(Decimal('0.0001'))),
        )


def truncate_visit_position(visit):
    visit = copy.copy(visit)
    visit['lat'], visit['long'] = truncate_position(visit['lat'], visit['long'])
    return visit


def save_visits(visits):
    for visit in visits:
        r.lpush('visits', json.dumps(visit))
        r.ltrim('visits', 0, 99)


def get_last_visits():
    return [json.loads(x.decode('utf-8')) for x in r.lrange('visits', 0, -1)]


def get_unique_visits(visits):
    grouped_visits = defaultdict(list)

    for visit in visits:
        grouped_visits[(visit['device'], visit['lat'], visit['long'], visit['time'])].append(visit)

    return sorted(
        [max(x, key=itemgetter('rssi')) for x in grouped_visits.values()],
        key=itemgetter('time')
    )


def group_visits_by_device(visits):
    grouped_visits = defaultdict(list)

    for visit in visits:
        grouped_visits[visit['device']].append(visit)

    return grouped_visits


def group_visits_by_position(visits):
    grouped_visits = defaultdict(list)

    for visit in visits:
        grouped_visits[(visit['lat'], visit['long'])].append(visit)

    return grouped_visits
