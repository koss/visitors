import logging
import sys

from flask import Flask

from visitors import api
from visitors import views
from visitors.config import Config

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger('app')


def create_app():
    app = Flask(__name__)

    app.config.from_object(Config)

    configure_views(app)

    return app


def configure_views(app):
    app.register_blueprint(views.bp)
    app.register_blueprint(api.bp, url_prefix='/API')
    app.register_blueprint(api.v1.bp, url_prefix='/API/v1')
