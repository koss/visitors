from marshmallow import Schema
from marshmallow import fields
from marshmallow import validates_schema
from marshmallow import ValidationError


NODE_TYPES = (
    NODE_TYPE_TRACKER,
    NODE_TYPE_INTERNET_PROVIDER,
    NODE_TYPE_MOBILE_STATION,
    NODE_TYPE_DRONE
) = range(1, 5)

NODE_TYPES_MAP = {
    NODE_TYPE_TRACKER: 'tracker',
    NODE_TYPE_INTERNET_PROVIDER: 'internet_provider',
    NODE_TYPE_MOBILE_STATION: 'mobile_station',
    NODE_TYPE_DRONE: 'dron',
}


class NodeTypeField(fields.Field):
    def _deserialize(self, value, attr, data):
        return {'value': value, 'title': NODE_TYPES_MAP[value]}


class VisitSchema(Schema):
    lat = fields.Float(required=True)
    long = fields.Float(required=True)
    time = fields.Integer(required=True)
    device = fields.String(required=True)
    rssi = fields.Integer(required=True)


class InputTrackSchema(Schema):
    id = fields.UUID()
    node_type = NodeTypeField(required=True)

    visits = fields.Nested(VisitSchema, many=True, required=True)

    @validates_schema
    def validate_node_type(self, data):
        if 'node_type' in data and data['node_type']['value'] not in NODE_TYPES:
            raise ValidationError(
                'File type must be one of these [{}] values.'.format(
                    ', '.join([str(x) for x in NODE_TYPES_MAP.keys()])
                ),
                ['node_type']
            )
