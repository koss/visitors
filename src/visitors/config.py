import os


class Config(object):
    DEBUG = os.environ.get('DEBUG', True)
    SERVER_NAME = os.environ.get('SERVER_NAME', None)
    SECRET_KEY = os.environ['SECRET_KEY']

    TOKEN = os.environ.get('TOKEN', 'TOKEN_FOR_TRACKER')
    ARCHIVE_SERVER = os.environ.get('ARCHIVE_SERVER', 'http://localhost:9090')
