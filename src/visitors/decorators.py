import functools

from flask import request
from flask import jsonify as flask_jsonify

from visitors.config import Config


"""
Originally was copied from:
https://github.com/miguelgrinberg/api-pycon2015/blob/master/api/decorators.py

"""


def jsonify():
    """
    This decorator generates a JSON response from a Python dictionary or
    a SQLAlchemy model.
    """
    def decorator(f):
        @functools.wraps(f)
        def wrapped(*args, **kwargs):
            rv = f(*args, **kwargs)
            status_or_headers = {}
            headers = None
            if isinstance(rv, tuple):
                rv, status_or_headers, headers = rv + (None,) * (3 - len(rv))
            if isinstance(status_or_headers, (dict, list)):
                headers, status_or_headers = status_or_headers, None
            if not isinstance(rv, dict):
                # assume it is a model, call its to_json() method
                rv = rv.to_json().data
            rv = flask_jsonify(rv)
            if status_or_headers is not None:
                rv.status_code = status_or_headers
            if headers is not None:
                rv.headers.extend(headers)
            return rv
        return wrapped
    return decorator


def is_token_valid(token):
    return token == Config.TOKEN


def tracker_login_required():
    """
    This decorator checks if token that tracker must send is valid
    """
    def decorator(f):
        @functools.wraps(f)
        def wrapped(*args, **kwargs):
            auth_token = request.headers.get('X-Auth-Token')

            if is_token_valid(auth_token):
                return f(*args, **kwargs)
            else:
                return {'status': 'forbidden'}, 403

        return wrapped
    return decorator
